﻿
using Pathfinding;
using System.Collections;
using UnityEngine;

public class AnimalManager : MonoBehaviour
{
    private RichAI richAI;
    private Seeker seeker;
    private Animator anim;
    private Vector3 destination;
    private AudioSource sound;
    private bool hasReachDestination;

    [SerializeField]
    private AudioClip[] animalClips;

    //-----------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------PRIVATE METHODS-------------------------------------------------------//
    //-----------------------------------------------------------------------------------------------------------------------------//
    private void Start()
    {
        sound = GetComponent<AudioSource>();
        seeker = GetComponent<Seeker>();
        anim = GetComponent<Animator>();
        richAI = GetComponent<RichAI>();

        Invoke("LaunchChangeState", 0);
    }

    private void Update()
    {
        float speed = richAI.desiredVelocity.magnitude / 2;
        anim.SetFloat("Forward", speed);

        Vector3 s = richAI.transform.InverseTransformDirection(richAI.velocity).normalized;
        float turn = s.x;
        anim.SetFloat("Turn", turn);

        float distance = Vector2.Distance(destination, transform.position);
        hasReachDestination = false;

        // if close to spot or going under water
        if (!hasReachDestination && (distance < 0.1f || speed < 0.01))
            hasReachDestination = true;
    }

    private void Move(float maxSpeed)
    {
        richAI.maxSpeed = maxSpeed;
        destination = transform.position + Vector3.right * (Random.Range(0, 6) - 3) * 6 + Vector3.forward * (Random.Range(0, 6) - 3) * 6;
        seeker.StartPath(transform.position, destination);
        hasReachDestination = false;
    }

    private void LaunchChangeState()
    {
        StartCoroutine(ChangeState());
    }

    private IEnumerator ChangeState()
    {
        // Reset animations
        anim.SetBool("Sitting", false);
        anim.SetBool("Eating", false);
        anim.SetBool("Special", false);
        anim.SetBool("Resting", false);
        anim.SetBool("Sleeping", false);

        // choose new anim
        int state = Random.Range(0, 7);
        switch (state)
        {
            case 0:
                Move(0.6f);
                while (!hasReachDestination)
                {
                    yield return null;
                }
                anim.SetBool("Sitting", true);
                break;
            case 1:
                Move(3);
                while (!hasReachDestination)
                {
                    yield return null;
                }
                anim.SetBool("Sitting", true);
                break;
            case 2:
                // only for animals with sound else something different
                anim.SetBool("Special", true);
                if (sound != null)
                {
                    int timeLeft = Random.Range(10, 20);
                    while (timeLeft >= 0.0f)
                    {
                        sound.clip = animalClips[Random.Range(0, animalClips.Length)];
                        sound.Play();

                        int randomTime = Random.Range(1, 5);
                        timeLeft -= randomTime;
                        yield return new WaitForSeconds(randomTime);
                    }
                }
                break;
            case 3:
                anim.SetBool("Eating", true);
                break;
            case 4:
                anim.SetBool("Sitting", true);
                break;
            case 5:
                anim.SetBool("Resting", true);
                break;
            case 6:
                anim.SetBool("Sleeping", true);
                break;
            default:
                break;
        }

        Invoke("LaunchChangeState", Random.Range(5, 20));
    }
}
